const router = require('express').Router();

const {login,register} = require('../controllers/userController');


//registration router
router.post('/register', register);

//login router
router.post('/login', login);

module.exports = router;