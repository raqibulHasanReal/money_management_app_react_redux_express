const validator = require('validator');

const validate = user => {
    let error = {};

    if (!user.email) {
        error.email = 'pls provide your email.'
    } else if (!validator.isEmail(user.email)) {
        error.email = 'pls provide a valid Email'
    }

    if (!user.password) {
        error.password = "pls provide a password"
    }

    return {
        error,
        isValid: Object.keys(error).length === 0
    }
}

module.exports = validate;