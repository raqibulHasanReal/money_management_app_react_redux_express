const validator = require('validator');

const validate = user=>{
    let error ={};
    
    if(!user.name){
        error.name = 'pls provide your name.'
    }
    
    if(!user.email){
        error.email = 'pls provide your email.'
    } else if(!validator.isEmail(user.email)){
        error.email = 'pls provide a valid Email'
    }
    
    if(!user.password){
        error.password = "pls provide a password"
    }else if(user.password.length < 6){
        error.password = "password must be greater or equel 6"
    }
    if(!user.confirmPassword){
        error.confirmPassword = 'pls provide confirm password'
    }else if(user.password != user.confirmPassword){
        error.confirmPassword = 'pasword Doesn\'t match'
    }

    return {
        error,
        isValid: Object.keys(error).length === 0
    }  
}

module.exports = validate;